package ma.octo.assignement.controller;

import ma.octo.assignement.Configjwt.JwtRequestFilter;
import ma.octo.assignement.Configjwt.WebSecurityConfig;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositeRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.web.UserController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@MockBeans({ @MockBean(WebSecurityConfig.class)
})
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    CompteRepository compteRepository ;

    @MockBean
    UtilisateurRepository utilisateurRepository ;

    @MockBean
    AuditService auditService ;

    @MockBean
    TransferRepository transferRepository ;
    @MockBean
    JwtRequestFilter jwtRequestFilter ;
    @MockBean
    MoneyDepositeRepository moneyDepositeRepository ;


    @Test
    void listAllUsers_thenVerifyRequest() throws Exception {
        mockMvc.perform(get("/lister_utilisateurs")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}
