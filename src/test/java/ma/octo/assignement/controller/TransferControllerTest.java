package ma.octo.assignement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.Configjwt.JwtRequestFilter;
import ma.octo.assignement.Configjwt.WebSecurityConfig;
import ma.octo.assignement.TestUtil;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.MoneyDepositException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositeRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.web.TransferController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import java.math.BigDecimal;


import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@MockBeans({ @MockBean(WebSecurityConfig.class)
})
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(controllers = TransferController.class)

public class TransferControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    CompteRepository compteRepository ;

    @MockBean
    UtilisateurRepository utilisateurRepository ;

    @MockBean
    AuditService auditService ;

    @MockBean
    TransferRepository transferRepository ;
    @MockBean
    JwtRequestFilter jwtRequestFilter ;
@MockBean
    MoneyDepositeRepository moneyDepositeRepository ;

    private Compte c1 = new Compte() ;
    private Compte c2 = new Compte() ;

    @BeforeEach
    public void init() {

        c1.setNrCompte("010000A000001000");
        c1.setSolde(BigDecimal.valueOf(199000));
        c2.setNrCompte("010000B025001000");
        c2.setSolde(BigDecimal.valueOf(141000));
    }


    @Test
    void givenMissingCmtEm_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(1000));
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()  ) ).thenReturn(null);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                            .content(TestUtil.convertObjectToJson(transferDto) )
                            .contentType(MediaType.APPLICATION_JSON))
                   .andExpect(status().isUnauthorized());


    }

    @Test
    void givenMissingCmtBn_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(1000));
        transferDto.setNrCompteEmetteur("010000A000001000");
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()  ) ).thenReturn(c1);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;
        mockMvc.perform(post("/executerTransfers")
                        .content(TestUtil.convertObjectToJson(transferDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

    }

    @Test
    void givenMissingCmtBnAndEm_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(1000));
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;
        mockMvc.perform(post("/executerTransfers")
                        .content(TestUtil.convertObjectToJson(transferDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

    }


    @Test
    void givenMissingBalance_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                            .content(TestUtil.convertObjectToJson(transferDto) )
                            .contentType(MediaType.APPLICATION_JSON))
                            .andExpect(status().isBadRequest());


    }

    @Test
    void givenMissingMotif_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("");
        transferDto.setMontant(BigDecimal.valueOf(100)) ;
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                    .content(TestUtil.convertObjectToJson(transferDto) )
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());


    }


    @Test
    void givenZeroBalanceValue_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(0)) ;
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                    .content(TestUtil.convertObjectToJson(transferDto) )
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());

    }

    @Test
    void givenMaxBalanceValue_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(10000000)) ;
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                    .content(TestUtil.convertObjectToJson(transferDto) )
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isUnavailableForLegalReasons());

    }

    @Test
    void givenMinBalanceValue_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(1)) ;
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                    .content(TestUtil.convertObjectToJson(transferDto) )
                    .contentType(MediaType.APPLICATION_JSON))
                  .andExpect(status().isUnavailableForLegalReasons());



    }

    @Test
    void givenBalanceValueGreaterThanBalance_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(60)) ;
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        c1.setSolde(BigDecimal.valueOf(50));
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                    .content(TestUtil.convertObjectToJson(transferDto) )
                    .contentType(MediaType.APPLICATION_JSON))
                            .andExpect(status().isUnavailableForLegalReasons()) ;
    }




    @Test
    void givenValues_whenMakeTransfer_thenVerifyCreatedRequest() throws Exception {
        TransferDto transferDto = new TransferDto();
        transferDto.setDate(null);
        transferDto.setMotif("dgf");
        transferDto.setMontant(BigDecimal.valueOf(1000)) ;
        transferDto.setNrCompteEmetteur("010000A000001000");
        transferDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteEmetteur()) ).thenReturn(c1);
        when( compteRepository.findByNrCompte(  transferDto.getNrCompteBeneficiaire()  ) ).thenReturn(c2);
        System.out.print(TestUtil.convertObjectToJson(transferDto) ) ;

            mockMvc.perform(post("/executerTransfers")
                    .content(TestUtil.convertObjectToJson(transferDto) )
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated()) ;
    }

    @Test
    void listAllTransfers_thenVerifyRequest() throws Exception {
        mockMvc.perform(get("/listDesTransferts")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }



}
