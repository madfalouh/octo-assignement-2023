package ma.octo.assignement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.Configjwt.JwtAuthenticationEntryPoint;
import ma.octo.assignement.Configjwt.JwtRequestFilter;
import ma.octo.assignement.Configjwt.JwtTokenUtil;
import ma.octo.assignement.Configjwt.WebSecurityConfig;
import ma.octo.assignement.TestUtil;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.MoneyDepositException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositeRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.web.MoneyDepositController;
import ma.octo.assignement.web.TransferController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.Filter;
import javax.transaction.Transactional;

import java.math.BigDecimal;


import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MoneyDepositController.class)
@MockBeans({ @MockBean(WebSecurityConfig.class)
})
@AutoConfigureMockMvc(addFilters = false)

public class MoneyDepositControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CompteRepository compteRepository ;

    @MockBean
    UtilisateurRepository utilisateurRepository ;

    @MockBean
    AuditService auditService ;

    @MockBean
    MoneyDepositDto moneyDepositDto ;
    @MockBean
    TransferRepository transferRepository ;
    @MockBean
    MoneyDepositeRepository moneyDepositeRepository ;
    @MockBean
    JwtRequestFilter jwtRequestFilter ;


    private Compte c1 = new Compte() ;


    @BeforeEach
    public void init() {

        c1.setNrCompte("010000A000001000");
        c1.setSolde(BigDecimal.valueOf(199000));




    }


    @Test
    void givenMissingNom_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(1000));
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()  ) ).thenReturn(c1);

        mockMvc.perform(post("/executerDeposites")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTY2NzE2ODU3OCwiaWF0IjoxNjY3MTUwNTc4fQ.mYLT3NX7bsACpEGkRhHGzkUm7GbLi4nt0DVMb_huehadwPM1sbbIOdYiJSHTE2Xi2Pu4ePYGe9PEKR9oLsrWzw")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest() );


    }

    @Test
    void givenEmptyNom_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto= new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(1000));
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        moneyDepositDto.setNomcompletetteur("");
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()  ) ).thenReturn(c1);
        System.out.print(TestUtil.convertObjectToJson(moneyDepositDto) ) ;

        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());


    }

    @Test
    void givenMissingCmtBn_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(1000));
        moneyDepositDto.setNomcompletetteur("reda");
        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

    }

    @Test
    void givenMissingCmtBnAndNom_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(1000));
        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

    }


    @Test
    void givenMissingBalance_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto= new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        moneyDepositDto.setNomcompletetteur("reda");
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()  ) ).thenReturn(c1);
        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());


    }

    @Test
    void givenMissingMotif_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("");
        moneyDepositDto.setMontant(BigDecimal.valueOf(100)) ;
        moneyDepositDto.setNomcompletetteur("reda");
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire() ) ).thenReturn(c1);

        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());


    }


    @Test
    void givenZeroBalanceValue_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(0)) ;
        moneyDepositDto.setNomcompletetteur("reda");
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire() ) ).thenReturn(c1);


        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    void givenMaxBalanceValue_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(10000000)) ;
        moneyDepositDto.setNomcompletetteur("reda");
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()  ) ).thenReturn(c1);
        System.out.print(TestUtil.convertObjectToJson(moneyDepositDto) ) ;

        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnavailableForLegalReasons());

    }

    @Test
    void givenMinBalanceValue_whenMakeDeposite_thenVerifyBadRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(1)) ;
        moneyDepositDto.setNomcompletetteur("reda");
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()  ) ).thenReturn(c1);
        System.out.print(TestUtil.convertObjectToJson(moneyDepositDto) ) ;

        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnavailableForLegalReasons());



    }


    @Test
    void givenValues_whenMakeDeposite_thenVerifyCreatedRequest() throws Exception {
        MoneyDepositDto moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setDate(null);
        moneyDepositDto.setMotif("dgf");
        moneyDepositDto.setMontant(BigDecimal.valueOf(1000)) ;
        moneyDepositDto.setNomcompletetteur("reda");
        moneyDepositDto.setNrCompteBeneficiaire("010000B025001000") ;
        c1.setId(3L);
        when( compteRepository.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()  ) ).thenReturn(c1);
        System.out.print(TestUtil.convertObjectToJson(moneyDepositDto) ) ;

        mockMvc.perform(post("/executerDeposites")
                        .content(TestUtil.convertObjectToJson(moneyDepositDto) )
                        .contentType(MediaType.APPLICATION_JSON).with(csrf()))

                .andExpect(status().isCreated()) ;
    }


    @Test
    void listAllDeposits_thenVerifyRequest() throws Exception {
        mockMvc.perform(get("/listDesDeposits")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}
