package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

    @Autowired
    private TransferRepository transferRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    private Utilisateur u ;
    private Utilisateur u2 ;

    private Compte c1 ;
    private Compte c2 ;

    @Test
  public void setup(){
        u=utilisateurRepository.findById(1L).get() ;
        u2=utilisateurRepository.findById(2L).get() ;
        c1= compteRepository.findById(3L).get() ;
        c2= compteRepository.findById(4L).get() ;
  }


  @Test
  public void save() {

    Transfer transfer = transferRepository.save(new Transfer(12L,new BigDecimal(12) , new Date() , c1 , c2 , "test" ));
    assertThat(transfer.getId()).isGreaterThan(0) ;

  }

    @Test
    public void findAll() {
        List<Transfer> l = transferRepository.findAll() ;
        assertThat(l.size()).isGreaterThan(0) ;
    }

    @Test
    public void findOne() {

        Transfer transfer1 = transferRepository.save(new Transfer(12L,new BigDecimal(12) , new Date() , c1 , c2 , "test" ));
        Transfer transfer2 = new Transfer(12L,new BigDecimal(12) , new Date() , c1 , c2 , "test2" );

        Example<Transfer> exemple1 = Example.of(transfer1);
        Optional<Transfer> a = transferRepository.findOne(exemple1) ;
        assertThat(a).isNotEmpty();
        Example<Transfer> exemple2 = Example.of(transfer2);
        Optional<Transfer> b = transferRepository.findOne(exemple2) ;
        assertThat(b).isEmpty();


    }

    @Test
    public void delete() {
        Transfer transfer1 = transferRepository.save(new Transfer(12L,new BigDecimal(12) , new Date() , c1 , c2 , "test" ));
         transferRepository.delete(transfer1);
         Example<Transfer> exemple = Example.of(transfer1);
         Optional<Transfer> a = transferRepository.findOne(exemple) ;
         assertThat(a).isEmpty() ;

    }

    @Test
    public void findById() {
        Optional<Transfer> transfer1 = transferRepository.findById(5L);
        Optional<Transfer> transfer2 = transferRepository.findById(200L);
        assertThat(transfer1).isNotEmpty() ;
        assertThat(transfer2).isEmpty() ;
    }



}