package ma.octo.assignement.repository;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class UserRepositoryTest {
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    private Utilisateur u = new Utilisateur() ;


    @Test
    public void findAll() {
        List<Utilisateur> l = utilisateurRepository.findAll() ;
        assertThat(l.size()).isGreaterThan(0) ;
    }

    @Test
    public void save() {
        u = utilisateurRepository.findById(1L).get();
        Utilisateur c = utilisateurRepository.save(u) ;
        assertThat(c.getId()).isGreaterThan(0) ;
    }

    @Test
    public void delete() {
        u.setId(10L);
        utilisateurRepository.delete(u);
        Example<Utilisateur> exemple = Example.of(u);
        System.out.print(exemple) ;
        Optional<Utilisateur> a = utilisateurRepository.findById(10L);
        assertThat(a).isEmpty() ;
    }

    @Test
    public void findById() {
        Optional<Utilisateur> a = utilisateurRepository.findById(2L);
        assertThat(a).isNotEmpty() ;
    }

}
