package ma.octo.assignement.repository;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class CompteRepositoryTest {
    private Compte c1 ;

    @Autowired
    private CompteRepository compteRepository;

    @Test
    public void findAll() {
        List<Compte> l = compteRepository.findAll() ;
        assertThat(l.size()).isGreaterThan(0) ;
    }

    @Test
    public void save() {
        c1= compteRepository.findById(3L).get() ;
        Compte c = compteRepository.save(c1) ;
        assertThat(c.getId()).isGreaterThan(0) ;
    }

    @Test
    public void delete() {
        c1= compteRepository.findById(3L).get() ;
        compteRepository.delete(c1);
        Example<Compte> exemple = Example.of(c1);
        System.out.print(exemple) ;
        Optional<Compte> a = compteRepository.findById(3L);
        assertThat(a).isEmpty() ;
    }

    @Test
    public void findById() {
        Optional<Compte> a = compteRepository.findById(3L);
        assertThat(a).isNotEmpty() ;
    }


}
