package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class MoneyDepositRepositoryTest {

    @Autowired
    private MoneyDepositeRepository moneyDepositRepository;
    @Autowired
    private CompteRepository compteRepository;



    private Compte c1 = new Compte() ;


    @BeforeEach
    public void setup(){
        c1= compteRepository.findById(3L).get() ;
    }


    @Test
    public void save() {

        MoneyDeposit deposit = moneyDepositRepository.save(new MoneyDeposit(12L,new BigDecimal(12) , new Date() , "reda" ,c1 ,  "test" ));
        assertThat(deposit.getId()).isGreaterThan(0) ;

    }

    @Test
    public void findAll() {
        List<MoneyDeposit> l = moneyDepositRepository.findAll() ;
        assertThat(l.size()).isEqualTo(1);
    }

    @Test
    public void findOne() {

        MoneyDeposit deposit1 = moneyDepositRepository.save(new MoneyDeposit(12L,new BigDecimal(12) , new Date() , "reda" ,c1 ,  "test" ));
        MoneyDeposit deposit2 = new MoneyDeposit(12L,new BigDecimal(12) , new Date() , "reda" ,c1 ,  "test2" ) ;

        Example<MoneyDeposit> exemple1 = Example.of(deposit1);
        Optional<MoneyDeposit> a = moneyDepositRepository.findOne(exemple1) ;
        assertThat(a).isNotEmpty();
        Example<MoneyDeposit> exemple2 = Example.of(deposit2);
        Optional<MoneyDeposit> b = moneyDepositRepository.findOne(exemple2) ;
        assertThat(b).isEmpty();


    }

    @Test
    public void delete() {
        MoneyDeposit deposit1 = moneyDepositRepository.save(new MoneyDeposit(12L,new BigDecimal(12) , new Date() , "reda" ,c1 ,  "test" ));
        moneyDepositRepository.delete(deposit1);
        Example<MoneyDeposit> exemple = Example.of(deposit1);
        Optional<MoneyDeposit> a = moneyDepositRepository.findOne(exemple) ;
        assertThat(a).isEmpty() ;

    }

    @Test
    public void findById() {
        Optional<MoneyDeposit> transfer1 = moneyDepositRepository.findById(5L);
        Optional<MoneyDeposit> transfer2 = moneyDepositRepository.findById(200L);
        assertThat(transfer1).isEmpty() ;
        assertThat(transfer2).isEmpty() ;
    }



}