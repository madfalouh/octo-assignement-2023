package ma.octo.assignement.exceptions;

public class MoneyDepositException extends Exception  {

    public MoneyDepositException() {
    }

    public MoneyDepositException(String message) {
        super(message);
    }

}
