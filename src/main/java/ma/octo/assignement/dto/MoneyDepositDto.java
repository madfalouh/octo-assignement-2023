package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class MoneyDepositDto {
    private String nomcompletetteur;
    private String nrCompteBeneficiaire;
    private String motif;
    private BigDecimal montant;
    private Date date;

    public String getNomcompletetteur() {
        return nomcompletetteur;
    }

    public String getNrCompteBeneficiaire() {
        return nrCompteBeneficiaire;
    }

    public String getMotif() {
        return motif;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public Date getDate() {
        return date;
    }

    public void setNomcompletetteur(String nomcompletetteur) {
        this.nomcompletetteur = nomcompletetteur;
    }

    public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MoneyDepositDto{" +
                "nomcompletetteur='" + nomcompletetteur + '\'' +
                ", nrCompteBeneficiaire='" + nrCompteBeneficiaire + '\'' +
                ", motif='" + motif + '\'' +
                ", montant=" + montant +
                ", date=" + date +
                '}';
    }
}
