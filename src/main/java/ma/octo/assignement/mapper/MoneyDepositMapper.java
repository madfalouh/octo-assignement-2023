package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.dto.TransferDto;

public class MoneyDepositMapper {

    private static MoneyDepositDto moneyDepositDto;

    public static MoneyDepositDto map(MoneyDeposit moneyDeposit) {
        moneyDepositDto = new MoneyDepositDto();
        moneyDepositDto.setNrCompteBeneficiaire(moneyDeposit.getCompteBeneficiaire().getNrCompte());
        moneyDepositDto.setDate(moneyDeposit.getDateExecution()  );
        moneyDepositDto.setMotif(moneyDeposit.getMotifDeposit()  );

        return moneyDepositDto;

    }

}
