package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.ComptenonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositeRepository;
import ma.octo.assignement.repository.TransferRepository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

public class TransferService {
    public static final int MONTANT_MAXIMAL = 10000;

    public void executeTransfer(CompteRepository rep1 , TransferRepository re2 , AuditService monservice , TransferDto transferDto) throws Exception {
        Compte c1 = rep1.findByNrCompte(transferDto.getNrCompteEmetteur());
        transferDto.setDate(new Date());
        System.out.print( "010000A000001000".equals(transferDto.getNrCompteEmetteur())  ) ;
        Compte f12 = rep1.findByNrCompte(transferDto.getNrCompteBeneficiaire());


        if (c1 == null || f12 ==null) {
            System.out.println("Compte Non existant");
            throw new ComptenonExistantException("Compte Non existant") ;
        }
        System.out.println("lolllml");
        if (Optional.ofNullable(transferDto.getMontant()).orElse(BigDecimal.valueOf(0)).intValue()==0  ) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().compareTo( BigDecimal.valueOf(10)) ==-1) {
            System.out.println("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) ==1 ) {
            System.out.println("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        else if ( transferDto.getMotif() == null  || transferDto.getMotif().length() == 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        else if (c1.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            System.out.println("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }



        c1.setSolde(new BigDecimal(c1.getSolde().intValue() - transferDto.getMontant().intValue()));
        rep1.save(c1);

        f12.setSolde(new BigDecimal(f12.getSolde().intValue() + transferDto.getMontant().intValue()));
        rep1.save(f12);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(f12);
        transfer.setCompteEmetteur(c1);
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setMotifTransfer(transferDto.getMotif());
        System.out.print(transfer.toString());


        re2.save(transfer);

        monservice.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
    }





}
