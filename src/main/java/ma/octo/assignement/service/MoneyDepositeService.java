package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.ComptenonExistantException;
import ma.octo.assignement.exceptions.MoneyDepositException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


@Service(value = "/service")
public class MoneyDepositeService {
    public static final int MONTANT_MAXIMAL = 10000;


    public BigDecimal SumOfMantantOnDay( MoneyDepositeRepository re2 ,  long id ) {

        return re2.listMaxMontant(id , new Date())  ;
    }

   public void executeDeposite(CompteRepository rep1 , MoneyDepositeRepository re2 , AuditService monservice ,  MoneyDepositDto moneyDepositDto) throws  Exception {

       Compte c1 = rep1.findByNrCompte(  moneyDepositDto.getNrCompteBeneficiaire()   );

       moneyDepositDto.setDate(new Date());
       System.out.print(moneyDepositDto) ;

       if (c1 == null) {
           System.out.println("Compte Non existant");
           throw new ComptenonExistantException("Compte Non existant");
       }

       if (Optional.ofNullable(moneyDepositDto.getMontant()).orElse(BigDecimal.valueOf(0)).intValue()==0) {
           System.out.println("Montant vide");
           throw new MoneyDepositException("Montant vide");
       } else if (moneyDepositDto.getMontant().compareTo(BigDecimal.valueOf( 10 ))==-1) {
           System.out.println("Montant minimal de transfer non atteint");
           throw new MoneyDepositException("Montant minimal de transfer non atteint");
       } else if (moneyDepositDto.getMontant().compareTo(BigDecimal.valueOf( MONTANT_MAXIMAL ))==1 ) {
           System.out.println("Montant maximal de transfer dépassé");
           throw new MoneyDepositException("Montant maximal de transfer dépassé");
       }
       else if ( moneyDepositDto.getMotif()==null || moneyDepositDto.getMotif().length() == 0) {
           System.out.println("Motif vide");
           throw new MoneyDepositException("Motif vide");
       }
       else if ( moneyDepositDto.getNomcompletetteur()==null || moneyDepositDto.getNomcompletetteur().length() == 0) {
           System.out.println("Nom vide");
           throw new MoneyDepositException("Nom vide");
       }

       BigDecimal sum = SumOfMantantOnDay(re2, c1.getId()) ;

       if (  sum !=null && (sum.add(moneyDepositDto.getMontant())).compareTo(BigDecimal.valueOf(10000))==1      ){
           System.out.println("Montant depase");
           throw new MoneyDepositException("montant du jour depase");
       }

       c1.setSolde(c1.getSolde().add(moneyDepositDto.getMontant()));
       rep1.save(c1);

       MoneyDeposit moneyDeposit = new MoneyDeposit();
       moneyDeposit.setDateExecution(moneyDepositDto.getDate());
       moneyDeposit.setCompteBeneficiaire(c1);
       moneyDeposit.setMontant(moneyDepositDto.getMontant());
       moneyDeposit.setMotifDeposit(moneyDepositDto.getMotif());
       moneyDeposit.setNom_prenom_emetteur(moneyDepositDto.getNomcompletetteur());

       re2.save(moneyDeposit);


       monservice.auditDeposit("Une deposite au compte " + moneyDepositDto.getNrCompteBeneficiaire()+ " par " + moneyDepositDto.getNomcompletetteur()
               + " d'un montant de " + moneyDepositDto.getMontant()
               .toString());

   }

}
