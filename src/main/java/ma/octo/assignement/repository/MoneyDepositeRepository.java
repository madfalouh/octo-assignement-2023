package ma.octo.assignement.repository;

import ma.octo.assignement.domain.MoneyDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface MoneyDepositeRepository extends JpaRepository<MoneyDeposit, Long> {


    @Query(value = " select SUM(MONTANT)  FROM DEP d where COMPTEBENEFICIAIRE_ID=?1  and DATEDIFF( dd,?2, dateExecution)=0  ;", nativeQuery = true)
    BigDecimal listMaxMontant(long id , Date date) ;

}
