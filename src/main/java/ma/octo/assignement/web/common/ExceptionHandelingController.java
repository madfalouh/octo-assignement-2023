package ma.octo.assignement.web.common;


import ma.octo.assignement.exceptions.ComptenonExistantException;
import ma.octo.assignement.exceptions.MoneyDepositException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ExceptionHandelingController {

    @ExceptionHandler(SoldeDisponibleInsuffisantException.class)
    public ResponseEntity<String> handleSoldeDisponibleInsuffisantException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Pas de solde pas de transfer", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
    }

    @ExceptionHandler(ComptenonExistantException.class)
    public ResponseEntity<String> handleCompteNonExistantException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<String> handleTransactionException(Exception ex, WebRequest request) {
        if(ex.getMessage().contains("Montant vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else if(ex.getMessage().contains("Motif vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else if(ex.getMessage().contains("Montant vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else if(ex.getMessage().contains("Montant maximal de transfer dépassé")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);

        }else if(ex.getMessage().contains("Montant minimal de transfer non atteint")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);

        }else {
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAUTHORIZED);

        }

    }

    @ExceptionHandler(MoneyDepositException.class)
    public ResponseEntity<String> handleMoneyDepositExceptionException(Exception ex, WebRequest request) {
        if(ex.getMessage().contains("Montant vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else if(ex.getMessage().contains("Motif vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else if(ex.getMessage().contains("Montant vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else if(ex.getMessage().contains("Montant maximal de transfer dépassé")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);

        }else if(ex.getMessage().contains("Montant minimal de transfer non atteint")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);

        }else if(ex.getMessage().contains("Nom vide")){
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.BAD_REQUEST);

        }else {
            return new ResponseEntity<>("Compte introuvable", null, HttpStatus.UNAUTHORIZED);

        }

    }




}
