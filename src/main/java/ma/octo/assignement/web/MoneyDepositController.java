package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;

import ma.octo.assignement.dto.MoneyDepositDto;

import ma.octo.assignement.exceptions.ComptenonExistantException;
import ma.octo.assignement.exceptions.MoneyDepositException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositeRepository;

import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.MoneyDepositeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController(value = "/moneyDeposites")
public class MoneyDepositController {




    Logger LOGGER = LoggerFactory.getLogger(MoneyDepositController.class);

    @Autowired
    private CompteRepository rep1;
    @Autowired
    private MoneyDepositeRepository re2;
    @Autowired
    private AuditService monservice;

    private final UtilisateurRepository re3;

    @Autowired
    MoneyDepositController(UtilisateurRepository re3) {
        this.re3 = re3;
    }

    @GetMapping("listDesDeposits")
    List<MoneyDeposit> loadAll() {
        LOGGER.info("Lister des utilisateurs");
        var all = re2.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all ;
        }
    }






    @PostMapping("/executerDeposites")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody MoneyDepositDto moneyDepositDto)
            throws Exception {
        MoneyDepositeService moneyDepositeService = new MoneyDepositeService() ;
        moneyDepositeService.executeDeposite( rep1 ,re2 ,monservice , moneyDepositDto);

    }




}
